'use strict'

// #1  Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

class ServerRequest {
    constructor() {
        this.root = document.querySelector('#root');
        this.USER = 'https://ajax.test-danit.com/api/json/users';
        this.POST = 'https://ajax.test-danit.com/api/json/posts';
    }

    async fetchData(url) {
        try {
            const response = await fetch(url);
            return response.json();
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    }

    async getUsersAndPosts() {
        const [userResponse, postResponse] = await Promise.all([
            this.fetchData(this.USER),
            this.fetchData(this.POST)
        ]);

        return [userResponse, postResponse];
    }

    renderCards(users, posts) {
        const fragment = this.createFragment(users, posts);
        this.root.append(fragment);
    }

    createFragment(users, posts) {
        const fragment = document.createDocumentFragment();
        users.forEach(user => {
            posts.forEach(post => {
                if (user.id === post.userId) {
                    const cardElement = this.createCardElement(user, post);
                    cardElement.dataset.postId = post.id;
                    fragment.append(cardElement);
                }
            });
        });
        return fragment;
    }

    createCardElement(user, post) {
        const card = document.createElement('div');
        card.classList.add('card');
        const userFullName = this.createUserFullName(user);
        const titlePost = this.createTitlePost(post);
        const textPost = this.createPostText(post);
        const deletePost = this.createDeletePostButton(post);
        card.append(userFullName, titlePost, textPost, deletePost);
        return card;
    }

    createUserFullName(user) {
        const userFullName = document.createElement('p');
        userFullName.classList.add('user-full-name');
        userFullName.innerHTML = ` ${user.name} ${user.username} <img class="img" src="./ok_green.png" alt=""> <span class="email">${user.email}</span>`;
        return userFullName;
    }

    createTitlePost(post) {
        const titlePost = document.createElement('p');
        titlePost.classList.add('title-post');
        titlePost.innerHTML = `<img class="img" src="./flag_white.png" alt=""> ${post.title}`;
        return titlePost;
    }

    createPostText(post) {
        const textPost = document.createElement('p');
        textPost.textContent = post.body;
        return textPost;
    }

    createDeletePostButton(post) {
        const deletePost = document.createElement('img');
        deletePost.src = './delete.png';
        deletePost.classList.add('delete-post');
        deletePost.addEventListener('click', () => {
            this.deletePost(post);
        });
        return deletePost;
    }

    async deletePost(post) {
        try {
            const response = await fetch(`${this.POST}/${post.id}`, {
                method: 'DELETE',
            });
            if (response.ok) {
                this.deleteCardElement(post);
            }
        } catch (error) {
            console.error("Error deleting post:", error);
        }
    }

    deleteCardElement(post) {
        const cardElement = document.querySelector(`.card[data-post-id="${post.id}"]`);
        if (cardElement) {
            cardElement.remove();
        }
    }
}

const serverRequest = new ServerRequest();

serverRequest.getUsersAndPosts().then(([users, posts]) => {
    serverRequest.renderCards(users, posts);
});
